# Simple web application blue-green deployment with GitLab CI and CodePipeline on an AWS ECS cluster

 My post on Medium about the project: https://link.medium.com/jIJeXOdenzb


## Pre-reqs

To follow along with this tutorial, you will need the following:
- A GitLab account with an active project.
- An AWS account with permissions to create resources, a DNS domain in Route53 and an S3 bucket for storing the Terraform backend.
- The Terraform CLI and AWS CLI are installed on your local machine.

## Building & Running

Clone the project to any directory where you do development work

```
git clone https://gitlab.com/Andr1500/cicd_bluegreen_codepipeline_ecs.git
```

## Creation of AWS infrastructure (ECR, CodeCommit, CodeBuild, CodeDeploy, CodePipeline, SNS topic, S3 bucket, ECS) with Terraform

For connectivity from Gitlab to CodeCommit and cloning repository it is necessary to set up an SSH connection to the CodeCommit repository, more information here: 
https://docs.aws.amazon.com/codecommit/latest/userguide/setting-up-ssh-unixes.html?icmpid=docs_acc_console_connect_np

Go to /env_aws_Codepipeline_ECS folder and build AWS environment:

set AWS credentials, credentials can be exported as environment variables:
```
export AWS_SECRET_ACCESS_KEY="SECRET_KEY"
export AWS_ACCESS_KEY_ID="ACCES_KEY"
```
run ```terraform init```
if everything is ok, run ```terraform plan``` and ```terraform apply```
when infrastructure will not be necessary, run ```terraform destroy```

## Gitlab pipeline

A working set of CI release Gitlab workflows are provided in .gitlab-ci.yml.

Gitlab pipeline stages:

- **test** Making SAST tests of some repository files.
- **push_to_codecommit** Clone existing repo to AWS Codecommit repo.

## Gitlab Variables

Add the variables into Gitlab repo Settings -> CI/CD -> Variables

![Gitlab Variables](images/variables_gitlab.png)

## AWS pipeline

Main part of CI/CD pipeline is done by AWS Codepipeline with stages:
Source - CodeCommit,
Build  - CodeBuild,
Deploy - CodeDeploy.
Application deploy on Elastic Container Service. SNS topic is configured for sending emails about success or fail stages of CodePipeline. S3 bucket is necessary for storing source and buid artifacts which are necesary for CodePipeline execution.

![AWS pipeline](images/codepipeline_waiting.png)

## AWS Infrastructure scema

![schema](images/codepipeline_ecs_schema1.png)

### [Gitlab pipeline](https://gitlab.com/Andr1500/cicd_bluegreen_codepipeline_ecs/-/pipelines)
