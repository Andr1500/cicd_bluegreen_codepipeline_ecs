#variables
variable "region" {
  default = "eu-central-1"
}

variable "aws_account_id" {
  default = "123456789"
}

variable "service_name" {
  type    = string
  default = "nginx"
}

variable "service_container" {
  default = "public.ecr.aws/docker/library/nginx:stable-alpine"
}

variable "container_port" {
  default = "80"
}

variable "memory_reserv" {
  default = "100"
}


variable "s3_bucket_name" {
  default = "codepipeline-bucket-stare-artifacts"
}

variable "instance_type" {
  default = "t3.micro"
}

variable "route53_hosted_zone_name" {
  default = "yourdomain.com"
}

variable "route53_subdomain_name" {
  default = "nginx-app"
}

variable "sns_endpoint" {
  default = "your_email"
}

variable "repository_name" {
  default = "from_gitlab"
}

variable "branch_name" {
  default = "main"
}
