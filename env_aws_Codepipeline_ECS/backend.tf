#############################
# Terraform backend
#############################

terraform {
  backend "s3" {
    bucket = "your_backend_s3_bucket"                # bucket for terraform state file, should be exist
    key    = "codepipeline-config/terraform.tfstate" # object name in the bucket to save terraform file
    region = "eu-central-1"                          # region where the bucket is created
  }
}
